import { buildFastify } from '~/buildFastify'

describe('Default route', () => {
  let app

  beforeAll(() => {
    app = buildFastify()
  })

  afterAll(async () => {
    await app.close()
  })

  it('should succeed', async () => {
    const { statusCode, payload } = await app.inject({
      method: 'GET',
      url: '/non-existent'
    })

    expect(statusCode).toBe(404)
  })
})
