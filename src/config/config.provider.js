import * as dotenv from 'dotenv'
import fs from 'fs'

class Config {
  constructor() {
    this.envConfig = dotenv.parse(fs.readFileSync(`.env`))
  }

  get(key) {
    return this.envConfig[key]
  }

  getDB() {
    return {
      subscription: {
        host: this.get('DB_HOST'),
        port: this.get('DB_PORT'),
        dbName: this.get('DB_NAME'),
        username: this.get('DB_USERNAME'),
        password: this.get('DB_PASSWORD')
      }
    }
  }
}

export const config = new Config()
