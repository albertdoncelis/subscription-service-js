import Sequelize from 'sequelize'
import { config } from '~/config/config.provider'

export const Op = Sequelize.Op

export const db = {}

const dbConfig = config.getDB()
Object.keys(dbConfig).forEach(database => {
  const { host, dbName, port, username, password } = dbConfig[database]

  db[database] = new Sequelize(dbName, username, password, {
    host,
    dialect: 'mysql',
    port,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    operatorsAliases: false,
    logging: false
  })
})
