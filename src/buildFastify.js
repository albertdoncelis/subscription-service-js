import fastify from 'fastify'
import { registerRoutes } from '@/fastify/registerRoutes'
import { checkAdmin } from '@/fastify/decorators/checkAdmin'
import { config } from '~/config/config.provider'

export function buildFastify(beforeRoutes) {
  const app = fastify({
    logger: JSON.parse(config.get('LOGGER'))
  })

  beforeRoutes && beforeRoutes(app)
  app.decorate('checkAdmin', checkAdmin)

  registerRoutes(app)

  return app
}
export default buildFastify
