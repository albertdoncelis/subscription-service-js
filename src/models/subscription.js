import { db } from '~/config/sequelize'
import * as Sequelize from 'sequelize'

const tableName = 'client'

export const Client = db.subscription.define(
  tableName,
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    client_id: Sequelize.STRING,
    username: Sequelize.STRING,
    first_name: Sequelize.STRING,
    last_name: Sequelize.STRING,
    email: Sequelize.STRING
  },
  {
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
  }
)
