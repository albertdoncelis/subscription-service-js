import { buildFastify } from '~/buildFastify'
import { registerSentry } from '@/fastify/registerSentry'
import { config } from '~/config/config.provider'
import { default as jwt } from 'fastify-jwt'

function registerJWT(app) {
  app.register(jwt, {
    secret: config.get('APP_JWT_SECRET')
  })
  app.addHook('onRequest', async (request, reply) => {
    await request.jwtVerify()
  })
}

export const createServer = async port => {
  const app = buildFastify(registerJWT)

  try {
    // setup error handling
    registerSentry(app, config.get('SENTRY_DSN'))

    await app.listen(port)
    return app
  } catch (e) {
    app.log.error(e)
    process.exit(1)
  }
}

export default createServer
