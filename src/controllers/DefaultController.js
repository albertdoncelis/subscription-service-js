export class DefaultController {
  static async get(request, reply) {
    reply.code(200).send({
      success: true,
      message: 'Welcome to the IG subscription service'
    })
  }
}
export default DefaultController
