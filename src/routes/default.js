import { DefaultController } from '~/controllers/DefaultController'

export function router(fastify, opts, next) {
  fastify.get('/', DefaultController.get)
  next()
}
export default router
