import { createServer } from '~/createServer'

const PORT = process.env.PORT || 3001

createServer(PORT)
