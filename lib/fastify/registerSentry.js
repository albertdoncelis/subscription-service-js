import * as Sentry from '@sentry/node'

export function registerSentry(fastify, dsn) {
  if (!dsn) {
    console.warn('Sentry DSN not provided') // eslint-disable-line
    return
  }

  Sentry.init({
    dsn,
    environment: process.env.NODE_ENV || ''
  })

  fastify.setErrorHandler(err => {
    Sentry.captureException(err)
  })
}
export default registerSentry
