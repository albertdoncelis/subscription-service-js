export async function checkAdmin(request, reply) {
  if (process.env.NODE_ENV !== 'test' && request.user.role !== 'admin') {
    reply.code(401).send({
      success: false,
      error: 'Unauthorized',
      message: 'You are not authorized to access this endpoint'
    })
  }
}
export default checkAdmin
